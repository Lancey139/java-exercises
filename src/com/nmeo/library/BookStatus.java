package com.nmeo.library;

public enum BookStatus {
    BORROWED,
    IN_LIBRARY,
    DESTROYED
}
