package com.nmeo.library;

public class LibraryMain {
    public static void main(String[] args) throws Exception {
        String relativePathToDataSetDirectory = args.length == 0 ? "/home/nmeo/workspace_nmeo/java-exercises/datasets/library/" : args[0];
        String csvFile = relativePathToDataSetDirectory + "/books.csv";
        String destroyString = relativePathToDataSetDirectory + "/destroy.txt";
        String[] borrowerStrings = new String[]{
            relativePathToDataSetDirectory + "/borrower1.txt",
            relativePathToDataSetDirectory + "/borrower2.txt",
            relativePathToDataSetDirectory + "/borrower3.txt",
            relativePathToDataSetDirectory + "/borrower4.txt"
        };
        SafeLibrary myLibrary = new SafeLibrary();

        LibraryCsvParser.parseBooksFile(csvFile, myLibrary);
        System.out.println(String.format("My library has %d unique books in total", myLibrary.getLibrarySize()));
        assert(myLibrary.getLibrarySize() == 10347);

        // Start measuring time from this point
        long start = System.currentTimeMillis();

        // Add the borrowers and destroyer threads here !

        long finish = System.currentTimeMillis();
        long timeElapsed = finish - start;

        System.out.println(String.format("My library has %d books borrowed", myLibrary.getBorrowedBooksCount()));
        System.out.println(String.format("My library has %d books in the library", myLibrary.getInLibraryBooksCount()));
        System.out.println(String.format("My library has %d books in total", myLibrary.getLibrarySize()));
        System.out.println(String.format("Your program took %dms to run", timeElapsed));
    }
}
