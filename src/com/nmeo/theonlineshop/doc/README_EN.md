# The Online Shop exercise

In this exercise you'll have to imagine the UML diagram of an online shop cli and then implement the application.

# Features description
TODO -> Translate from french version

# Generate SVG from GV files

To generate the svg files from gv, run the following script (update input and output parameters)
```bash
sudo apt install graphviz
dot -Tpng onlineshopcli.gv -o onlineshopcli.png
```
