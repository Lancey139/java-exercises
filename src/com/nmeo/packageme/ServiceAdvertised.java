package com.nmeo.packageme;

import com.nmeo.packageme.Constants;

/**
 * The ServiceAdvertised class represents a service that is advertised, and it extends the Service class.
 * It inherits attributes and methods from the Service class and has additional methods and overrides.
 */
public class ServiceAdvertised extends Service {

    /**
     * Constructor to create a ServiceAdvertised object with the given parameters.
     *
     * @param pName           The name of the service.
     * @param pDescription    The description of the service.
     * @param pPricePerHour   The price per hour of the service.
     * @param pDurationH      The duration in hours of the service.
     */
    public ServiceAdvertised(String pName, String pDescription, double pPricePerHour, int pDurationH) {
        super(pName, pDescription, pPricePerHour, pDurationH);
    }

    /**
     * Overrides the toString() method to return a string representation of the ServiceAdvertised object.
     * The string representation includes the separator constant, name, description, price per hour, duration,
     * and another separator constant.
     *
     * @return A string representation of the ServiceAdvertised object.
     */
    @Override
    public String toString() {
        return String.format("\n%s\n%s\n  -%s\n  -%.2f$/h\n  -%d hour(s)\n%s\n",
                Constants.SEPARATOR,
                mName,
                mDescription,
                getPricePerHour(),
                getDurationH(),
                Constants.SEPARATOR);
    }
}
