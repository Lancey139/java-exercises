package com.nmeo.packageme;

/**
 * This interface represents an online shop that allows users to interact with products and services,
 * including adding them to a basket, restocking products, displaying content, and purchasing items.
 * It also defines a constant for the basket size.
 */
public interface IOnlineShop {
    /**
     * Constant for the maximum size of the basket.
     */
    int BASKET_SIZE = 10;

    /**
     * Adds a product to the online shop.
     *
     * @param pName        The name of the product.
     * @param pDescription The description of the product.
     * @param pPrice       The price of the product.
     * @param pStock       The initial stock quantity of the product.
     */
    void addProduct(String pName, String pDescription, double pPrice, int pStock);

    /**
     * Adds a service to the online shop.
     *
     * @param pName             The name of the service.
     * @param pDescription      The description of the service.
     * @param pPricePerHour     The price per hour of the service.
     */
    void addService(String pName, String pDescription, double pPricePerHour);

    /**
     * Restocks a product in the online shop.
     *
     * @param pIndex    The index of the product in the product list.
     * @param pQuantity The quantity to restock.
     */
    void restockProduct(int pIndex, int pQuantity);

    /**
     * Displays the content of the basket.
     */
    void displayBasketContent();

    /**
     * Displays the list of products.
     */
    void displayProductsList();

    /**
     * Displays the list of services.
     */
    void displayServicesList();

    /**
     * Adds a product to the basket.
     *
     * @param pIndex The index of the product in the product list.
     */
    void addProductToBasket(int pIndex);

    /**
     * Adds a service to the basket.
     *
     * @param pIndex     The index of the service in the service list.
     * @param pDurationH The duration of the service in hours.
     */
    void addServiceToBasket(int pIndex, int pDurationH);

    /**
     * Removes an article from the basket.
     *
     * @param pIndex The index of the article in the basket.
     */
    void removeArticalFromBasket(int pIndex);

    /**
     * Purchases the items in the basket.
     */
    void purchaseItems();
}
