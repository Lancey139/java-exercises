package com.nmeo.packageme;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import com.nmeo.packageme.Constants;
import com.nmeo.packageme.Article;

/**
 * The Basket class represents a basket in an online shop that can hold articles.
 * It provides methods to add, remove, and display articles in the basket, as well as calculate the total price.
 */
public class Basket {
    private ArrayList<Article> mArticles;
    private int mMaxSize;

    /**
     * Constructor to create a Basket object with the given maximum size.
     *
     * @param pMaxSize The maximum size of the basket.
     */
    public Basket(int pMaxSize) {
        mArticles = new ArrayList<Article>();
        mMaxSize = pMaxSize;
    }

    /**
     * Adds an article to the basket, if the basket is not full.
     *
     * @param pArticle The article to be added to the basket.
     * @return true if the article is added successfully, false if the basket is full.
     */
    public boolean addArticle(Article pArticle) {
        if(mArticles.size() < mMaxSize) {
            mArticles.add(pArticle);
            return true;
        }

        System.err.println(Constants.ANSI_RED + "The basket is full" + Constants.ANSI_RESET);
        return false;
    }

    /**
     * Removes an article from the basket based on the index.
     *
     * @param pIndex The index of the article to be removed.
     * @return An Optional of the UUID of the removed article, or Optional.empty() if the index is invalid.
     */
    public Optional<UUID> removeArticle(int pIndex) {
        if(pIndex > mArticles.size()-1) {
            System.err.println(Constants.ANSI_RED + "Invalid index" + Constants.ANSI_RESET);
            return Optional.empty();
        }
        UUID result = mArticles.get(pIndex).getUuid();
        System.out.println(Constants.ANSI_GREEN + String.format("Article %s removed from basket", mArticles.get(pIndex)) + Constants.ANSI_RESET);
        mArticles.remove(pIndex);
        return Optional.of(result);
    }

    /**
     * Removes all articles from the basket.
     */
    public void removeAllArticle() {
        mArticles.clear();
    }

    /**
     * Calculates the total price of all articles in the basket.
     *
     * @return The total price of all articles in the basket.
     */
    public double basketPrice() {
        double totalPrice = mArticles.stream().mapToDouble(Article::getPrice).sum();
        return totalPrice;
    }

    /**
     * Displays the articles in the basket, including the total price.
     */
    public void displayArticles() {
        System.out.println(String.format("Your basket currently has %d articles", mArticles.size()));
        System.out.println(String.format("Total : %.2f$", basketPrice()));
        for(int i = 0; i< mArticles.size(); i++) {
           System.out.println(String.format("%d-%s", i, mArticles.get(i).toString()));
        }
    }
}
