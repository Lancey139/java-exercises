package com.nmeo.packageme;

/**
 * The Service class represents a service that extends the Article class.
 * It has additional attributes and methods specific to a service.
 */
public class Service extends Article {
    private int mDurationH;
    private double mPricePerHour;

    /**
     * Constructor to create a Service object with the given parameters.
     *
     * @param pName           The name of the service.
     * @param pDescription    The description of the service.
     * @param pPricePerHour   The price per hour of the service.
     * @param pDurationH      The duration in hours of the service.
     */
    public Service(String pName, String pDescription, double pPricePerHour, int pDurationH) {
        super(pName, pDescription, 0);
        mDurationH = pDurationH;
        mPricePerHour = pPricePerHour;
    }

    /**
     * Copy constructor to create a Service object by copying the attributes of another Service object.
     *
     * @param pServiceToCopy The Service object to be copied.
     */
    public Service(Service pServiceToCopy) {
        // We can access the pArticleToCopy's private attributes
        // because we are inside Article class :)
        this(pServiceToCopy.mName, pServiceToCopy.mDescription, pServiceToCopy.mPricePerHour, pServiceToCopy.mDurationH);
    }

    /**
     * Returns the price per hour of the service.
     *
     * @return The price per hour of the service.
     */
    public double getPricePerHour() {
        return mPricePerHour;
    }

    /**
     * Returns the duration in hours of the service.
     *
     * @return The duration in hours of the service.
     */
    public int getDurationH() {
        return mDurationH;
    }

    /**
     * Sets the duration in hours of the service.
     *
     * @param pDurationH The duration in hours of the service.
     */
    public void setDurationH(int pDurationH) {
        mDurationH = pDurationH;
    }

    /**
     * Computes the price of the service by multiplying the duration with the price per hour.
     */
    public void computePrice() {
        mPrice = mDurationH * mPricePerHour;
    }

    /**
     * Overrides the toString() method to return a string representation of the service.
     *
     * @return A string representation of the service.
     */
    @Override
    public String toString() {
        return String.format("%s\n  -%s\n  -%.2f$/h\n  -%d hour(s)", mName, mDescription, mPricePerHour, mDurationH);
    }
}
