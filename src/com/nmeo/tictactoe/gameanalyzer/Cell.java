package com.nmeo.tictactoe.gameanalyzer;

import java.util.Optional;


public class Cell {
    private final int mX;
    private final int mY;
    private Tocken mTocken;

    Cell(int pX, int pY) {
        mX = pX;
        mY = pY;
    }

    public static Optional<Cell> createCellFromString(String pEncodedCoordinates) {
        String [] coordinates = pEncodedCoordinates
            .replace("(","")
            .replace(")","")
            .split(",");
        if(coordinates.length != 2) {
            return Optional.empty();
        }
        try {
            return Optional.of(new Cell(Integer.parseInt(coordinates[0]), Integer.parseInt(coordinates[1])));
        } catch(Exception e) {
            return Optional.empty();
        }

    }

    public void setTocket(Tocken pTocken) {
        mTocken = pTocken;
    }

    public Tocken getTocket() {
        return mTocken;
    }

    public int getX() {
        return mX;
    }
    public int getY() {
        return mY;
    }

    @Override
    public String toString() {
        if(mTocken == null) {
            return " ";
        }
        switch(mTocken) {
            case X:
                return "X";
            case O:
                return "O";
            default:
                return " ";
        }
    }
}