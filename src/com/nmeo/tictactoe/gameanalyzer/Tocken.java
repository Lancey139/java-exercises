package com.nmeo.tictactoe.gameanalyzer;

public enum Tocken {
    X,
    O;


    public static String tockenString(Tocken pTocken) {
        if(pTocken == null) {
            return " ";
        }
        switch(pTocken) {
            case X:
                return "X";
            case O:
                return "O";
            default:
                return " ";
        }
    }
}