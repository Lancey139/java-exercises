# The Tic Tac Toe game analyzer

In this exercise you'll find content to train your collection's skills.

# Use Case 1 : Display the game

As a user, I want to vizualize a game described by a .csv file turn by turn. The game will be displayed on my terminal.
- The game's dataset will be chosen as a parameter
- Each time I press enter, the next move should appear.

## Dataset description

The first column indicates player X cell coordinates. The second column indicates player O cell.
Each line describe a turn in the game. The first coordinate is the row number, second one is the column.

Exemple:

```csv
(2,2);(1,3)
(3,3);(1,1)
```

```
    | 1 | 2 | 3 |
| 1 | O |   | O |
| 2 |   | X |   |
| 3 |   |   | X |
```

# Use Case 2 : Show the winner

As a user, I want to visualize the winner's tocken at the end of the game.